It looks like every available groovy version (1.8.9, 2.0.8, 2.1.9, 2.2.1) all have issues with generating bytecode for classes that extend interfaces that have default method implementations (new in Java 8).

Under Java 7, `./gradlew clean build` succeeds. Under Java 8, it fails with a VerifyError:

```
java.lang.VerifyError: (class: org/mpierce/groovy/GroovySetSubclass, method: super$1$parallelStream signature: ()Ljava/util/stream/Stream;) Illegal use of nonvirtual function call
	at java.lang.Class.forName0(Native Method)
	at java.lang.Class.forName(Class.java:259)
	at org.mpierce.groovy.GroovySetSubclassTest.class$(GroovySetSubclassTest.groovy)
	at org.mpierce.groovy.GroovySetSubclassTest.$get$$class$org$mpierce$groovy$GroovySetSubclass(GroovySetSubclassTest.groovy)
	at org.mpierce.groovy.GroovySetSubclassTest.testGroovySubclass(GroovySetSubclassTest.groovy:9)
```

You can control the `-source`/`-target` java versions with the gradle parameter `javaVersion` and the groovy version with `groovyVersion`, like so:

```
./gradlew clean build -PjavaVersion=1.5 -PgroovyVersion=1.8.6
```

It doesn't appear to matter which `-source` version is used, but I added that since there used to be a bug where `-source 1.5` produced a VerifyError under JDK8, and I wanted to verify that that wasn't the problem.

The build prints out the active java and groovy versions so you can easily tell which versions a given build used.
